﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private int animationSpritesIndex;
    private float frameCounter;


    //Save sprite var to SpriteRenderer
    private SpriteRenderer spriteRenderer;

    public int frameRate = 12;
    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<DataPlayer>().Sprites;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = animationSprites[0];
        Debug.Log($"{spriteRenderer.sprite.name}");
        animationSpritesIndex = 0;
        frameCounter = 0;

    }

    // Update is called once per frame
    void Update()
    {
        spriteRenderer.sprite = animationSprites[animationSpritesIndex];

        frameCounter++;

        //Conditions of currentFrame: % = 0 ->to change frame. Reset the count at 12?
        if (frameCounter > frameRate) frameCounter = 0;
        if (frameCounter % frameRate == 0) animationSpritesIndex++;

        //Check animationSpritesIndex
        if (animationSpritesIndex >= animationSprites.Length) animationSpritesIndex = 0;


    }
}
