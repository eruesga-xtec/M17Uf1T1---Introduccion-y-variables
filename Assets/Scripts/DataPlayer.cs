﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Assesin,
    Bard,
    Barbarian
}

public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    private string Name;
    public string Surname;
    public float Speed = 10f;
    public float Height;
    public float Weight;
    public float WalkDistance;

    public PlayerKind Kind;

    public Sprite[] Sprites;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"Hola {name}!");
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
